//
//  AppDelegate.h
//  aabb
//
//  Created by cxb on 16/1/11.
//  Copyright © 2016年 chuxiaobo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

